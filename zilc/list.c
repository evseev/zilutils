/*
 * list.c
 *
 * Copyright (C) 2015 Alexander Andrejevic <theflash AT sdf DOT lonestar DOT org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "list.h"

void list_append(list_t *list, list_t *element)
{
    element->next = list;
    element->prev = list->prev;
    element->next->prev = element->prev->next = element;
}

void list_prepend(list_t *list, list_t *element)
{
    element->next = list->next;
    element->prev = list;
    element->next->prev = element->prev->next = element;
}

void list_remove(list_t *element)
{
    element->next->prev = element->prev;
    element->prev->next = element->next;
}

void init_list(list_t *list)
{
    list->next = list->prev = list;
}
