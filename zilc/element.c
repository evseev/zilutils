/*
 * element.c
 *
 * Copyright (C) 2015 Alexander Andrejevic <theflash AT sdf DOT lonestar DOT org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <stdlib.h>
#include "element.h"

element_t *create_atom(const char *name, int global)
{
    element_t *element = (element_t*)malloc(sizeof(element_t));
    if (element == NULL) return NULL;

    element->type = TYPE_ATOM;
    element->atom.name = name;
    element->atom.global = global;

    return element;
}

element_t *create_string(const char *value)
{
    element_t *element = (element_t*)malloc(sizeof(element_t));
    if (element == NULL) return NULL;

    element->type = TYPE_STRING;
    element->string.value = value;

    return element;
}

element_t *create_number(double value)
{
    element_t *element = (element_t*)malloc(sizeof(element_t));
    if (element == NULL) return NULL;

    element->type = TYPE_NUMBER;
    element->number.value = value;

    return element;
}

element_t *create_tuple(list_t *list)
{
    element_t *element = (element_t*)malloc(sizeof(element_t));
    if (element == NULL) return NULL;

    element->type = TYPE_TUPLE;
    element->tuple.list = *list;

    return element;
}

element_t *create_statement(const char *name, list_t *parameters)
{
    element_t *element = (element_t*)malloc(sizeof(element_t));
    if (element == NULL) return NULL;

    element->type = TYPE_STATEMENT;
    element->statement.name = name;
    element->statement.parameters = *parameters;

    return element;
}
