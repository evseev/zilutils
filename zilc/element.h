/*
 * element.h
 *
 * Copyright (C) 2015 Alexander Andrejevic <theflash AT sdf DOT lonestar DOT org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef __ELEMENT_H__
#define __ELEMENT_H__

#include "list.h"

enum element_type
{
    TYPE_ATOM,
    TYPE_STRING,
    TYPE_NUMBER,
    TYPE_TUPLE,
    TYPE_STATEMENT
};

typedef struct element
{
    list_t link;
    int type;

    union
    {
        struct
        {
            const char *name;
            int global;
        } atom;

        struct
        {
            const char *value;
        } string;

        struct
        {
            double value;
        } number;

        struct
        {
            list_t list;
        } tuple;

        struct
        {
            const char *name;
            list_t parameters;
        } statement;
    };
} element_t;


element_t *create_atom(const char *name, int global);
element_t *create_string(const char *value);
element_t *create_number(double value);
element_t *create_tuple(list_t *list);
element_t *create_statement(const char *name, list_t *parameters);

#endif
