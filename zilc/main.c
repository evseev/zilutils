/*
 * main.c
 *
 * Copyright (C) 2015 Alexander Andrejevic <theflash AT sdf DOT lonestar DOT org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

int main(int argc, char *argv[], char *envp[])
{
    int verbose = 0;
    extern FILE *yyin;

    while (1)
    {
        int option_index;
        static struct option long_options[] =
        {
            { "verbose", no_argument, NULL, 'v'},
            { NULL,      0,           NULL, 0  }
        };

        int choice = getopt_long(argc, argv, "v", long_options, &option_index);
        if (choice == -1) break;

        switch (choice)
        {
        case 'v':
            verbose++;
            break;
        }
    }

    if (optind == argc)
    {
        fprintf(stderr, "No input files specified.\n");
        return EXIT_FAILURE;
    }

    while (optind < argc)
    {
        char *filename = argv[optind++];

        yyin = fopen(filename, "r");
        if (yyin == NULL)
        {
            fprintf(stderr, "Cannot open input file: %s\n", filename);
            continue;
        }

        switch (yyparse())
        {
        case 0:
            if (verbose) fprintf(stdout, "%s: Done parsing.\n", filename);
            break;

        case 1:
            fprintf(stderr, "%s: errors encountered.", filename);
            break;

        case 2:
            fprintf(stderr, "%s: out of memory.\n", filename);
            break;
        }
    }

    return EXIT_SUCCESS;
}
