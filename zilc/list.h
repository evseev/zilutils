/*
 * list.h
 *
 * Copyright (C) 2015 Alexander Andrejevic <theflash AT sdf DOT lonestar DOT org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef __LIST_H__
#define __LIST_H__

typedef struct list
{
    struct list *next, *prev;
} list_t;

void list_append(list_t *list, list_t *element);
void list_prepend(list_t *list, list_t *element);
void list_remove(list_t *element);
void init_list(list_t *list);

#endif
