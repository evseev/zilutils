/*
 * scanner.l
 *
 * Copyright (C) 2015 Alexander Andrejevic <theflash AT sdf DOT lonestar DOT org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

%{

#include <stdlib.h>
#include "element.h"
#include "parser.h"

static char *stripquotes(const char *string);

%}

%option noyywrap
%option case-insensitive

atomchar [^\x00-\x20,#';%()\[\]\{\}<>"\\.]

%%

;                                       { return SEMICOLON; }
\<                                      { return LEFT_CHEVRON; }
\>                                      { return RIGHT_CHEVRON; }
\(                                      { return LEFT_PARENTHESIS; }
\)                                      { return RIGHT_PARENTHESIS; }
,                                       { return COMMA; }
\*[0-7]+\*                              { yylval.number = strtol(&yytext[1], NULL, 8); return NUMBER; }
[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?  { yylval.number = strtod(yytext, NULL); return NUMBER; }
{atomchar}({atomchar}|\.)*              { yylval.string = strdup(yytext); return ATOM; }
\"[^"]*\"                               { yylval.string = stripquotes(yytext); return STRING; }
[ \r\n\t\f]+                            { /* whitespace */ }

%%

static char *stripquotes(const char *string)
{
    return strndup(&string[1], strlen(string) - 2);
}
