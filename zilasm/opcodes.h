/*
 * opcodes.h -- part of ZilUtils/ZilAsm
 *
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 *
 * Written at 2015 by Ilya Evseev <ilya.evseev@gmail.com>
 * Based on ZILF (c) 2010, 2015 Jesse McGrew
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef ZILASM_OPCODES
#define ZILASM_OPCODES 1

#include "symtable.h"

typedef enum {
	Zop_none    =   0,
	Zop_store   =   1,   // ..stores a result
	Zop_branch  =   2,   // ..branches to a label
	Zop_extra   =   4,   // ..takes an extra operand type byte, for a total of 8 possible operands
	Zop_varargs =   8,   // ..is nominally 2OP but can take up to 4 operands
	Zop_string  =  16,   // ..has a string literal operand
	Zop_label   =  32,   // ..can take a local label operand
	Zop_indvar  =  64,   // ..first operand is an indirect variable number
	Zop_call    = 128,   // ..first operand is a packed routine address
	Zop_term    = 256    // ..control flow does not pass to the following instruction
} ZOpcode_flags;

typedef struct {
	unsigned opcode;
	ZOpcode_flags flags;
} ZOpcode;

extern Symtable *Opcodes;

void init_opcodes(int version, int inform_syntax);

#endif  /* ifndef ZILASM_OPCODES */
