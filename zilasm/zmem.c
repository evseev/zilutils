/*
 * zmem.c -- part of ZilUtils/ZilAsm
 *
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 *
 * Written at 2015 by Ilya Evseev <ilya.evseev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <assert.h>
#include <strings.h>  /* bzero */
#include <stdlib.h>   /* malloc, free */

#include "zmem.h"

ZMemblock* zmem_init(unsigned maxsize)
{
	ZMemblock *zmb = malloc(sizeof(ZMemblock) + maxsize - 1);
	assert(zmb);
	zmb->allocated_size = maxsize;
	zmb->used_size = 0;
	bzero(&zmb->contents, maxsize);
	return zmb;
}

void zmem_destroy(ZMemblock *zmb)
{
	assert(zmb);
	free(zmb);
}

void zmem_putbyte(ZMemblock *zmb, unsigned char val)
{
	assert(zmb);
	assert(zmb->used_size < zmb->allocated_size);
	zmb->contents[zmb->used_size++] = val;
}
