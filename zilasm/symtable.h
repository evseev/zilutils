/*
 * symtable.h -- part of ZilUtils/ZilAsm
 *
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 * Written at 2015 by Ilya Evseev <ilya.evseev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef ZILASM_SYMTABLE
#define ZILASM_SYMTABLE 1

typedef struct {
	unsigned elems_count;
	unsigned name_size;
	unsigned elem_size;
	char contents[1];
} Symtable;

Symtable* symtable_create (unsigned elems_count, unsigned name_size, unsigned elem_size);
void*     symtable_lookup (const Symtable*, const char *name);
void*     symtable_lookup2(const Symtable*, const char *name, unsigned namelen);
void*     symtable_add    (Symtable*, const char *name, void *contents);
void*     symtable_add2   (Symtable*, const char *name, unsigned namelen, void *contents);
void      symtable_sort   (Symtable*);
void      symtable_destroy(Symtable*);

#endif  /* ifndef ZILASM_SYMTABLE */
