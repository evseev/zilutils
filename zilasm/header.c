/*
 * header.c -- part of ZilUtils/ZilAsm
 *
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 * Written at 2015 by Ilya Evseev <ilya.evseev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <string.h>  /* bzero */
#include <assert.h>

#include "header.h"

Program_header_struct Program_header;

const unsigned MAX_HEADER_LEN = 40;

void program_header_reset(unsigned zversion)
{
	bzero(&Program_header, sizeof(Program_header));
	Program_header.version = zversion;
}

ZMemblock *program_header_build(void)
{
	ZMemblock *zmb = zmem_init(MAX_HEADER_LEN);
	zmem_putbyte(zmb, Program_header.version);
	/* TODO */
	return zmb;
}
