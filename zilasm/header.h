/*
 * header.h -- part of ZilUtils/ZilAsm
 *
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 * Written at 2015 by Ilya Evseev <ilya.evseev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef ZILASM_HEADER
#define ZILASM_HEADER 1

#include "zmem.h"

typedef unsigned long   Byte_address;
typedef unsigned long   Word_address;
typedef unsigned long Packed_address;
typedef unsigned long        Offset8;
typedef unsigned int            Word;
typedef   signed char           Byte;
typedef          int            Bool;

typedef struct {
	unsigned version;  // 1..6
	// [$01] V1..V3: Flags1
	Bool  statusline_type;      // bit1: 0=score/turns, 1=hh:mm
	Bool  split_two_discs;      // bit2
	Bool  statusline_notavail;  // bit4
	Bool  screensplit_avail;    // bit5
	Bool  varpitchfont_default; // bit6
	// [$01] V4: Flags1
	Bool  colors_avail;         // v5: bit0
	Bool  pics_avail;           // v6: bit1
	Bool  bold_avail;           // v4: bit2
	Bool  italic_avail;         // v4: bit3
	Bool  fixedspace_avail;     // v4: bit4
	Bool  sound_avail;          // v6: bit5
	Bool  timedkeyb_avail;      // v4: bit7
	// Addresses
	Byte_address   highmem_base;  // [$04]
	Byte_address   start_pc;      //  [$06], v1
	Packed_address start_routine; //  [$06], v6
	Byte_address   dictionary;    // [$08]
	Byte_address   objects;       // [$0A]
	Byte_address   globals;       // [$0C]
	Byte_address   static_base;   // [$0E]
	// [$10] Flags2
	Bool  transcript_on;        // v1: bit0
	Bool  print_fixedfont;      // v3: bit1
	Bool  request_redraw;       // v6: bit2
	Bool  want_pics;            // v5: bit3
	Bool  want_undo;            // v5: bit4
	Bool  want_mouse;           // v5: bit5
	Bool  want_colors;          // v5: bit6
	Bool  want_sound;           // v5: bit7
	Bool  want_menus;           // v5: bit8
	//
	Byte_address  abbrevs;        // [$18], v2
	Word  file_length;            // [$1A], v3
	Word  checksum;               // [$1C], v3
	Byte  interpreter_number;     // [$1E], v4
	Byte  interpreter_version;    // [$1F], v4
	// Screen
	Byte  screen_lines;           // [$20], v4 ($FF = infinite)
	Byte  screen_chars;           // [$21], v4
	Word  screen_width;           // [$22], v5
	Word  screen_height;          // [$24], v5
	Byte  font_width;             // [$26], v5/v6
	Byte  font_height;            // [$27], v5/v6
	// Tables
	Offset8  routines;            // [$28], v6
	Offset8  strings;             // [$2A], v6
	Byte     backcolor;           // [$2C], v5
	Byte     forecolor;           // [$2D], v5
	Byte_address  term_chartable; // [$2E], v5
	Word     width3;              // [$30], v6
	Word     revnumber;           // [$32], v1
	Byte_address  alphabet;       // [$34], v5
	Byte_address  header_ext;     // [$36], v5
} Program_header_struct;

extern Program_header_struct Program_header;

void program_header_reset(unsigned zversion);
ZMemblock *program_header_build(void);

#endif  /* ifndef ZILASM_HEADER */
