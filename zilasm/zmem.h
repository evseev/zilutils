/*
 * zmem.h -- part of ZilUtils/ZilAsm
 *
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 *
 * Written at 2015 by Ilya Evseev <ilya.evseev@gmail.com>
 * Based on ZILF (c) 2010, 2015 Jesse McGrew
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef ZILASM_ZMEM
#define ZILASM_ZMEM 1

typedef struct {
	unsigned allocated_size;
	unsigned used_size;
	unsigned char contents[1];
} ZMemblock;

extern ZMemblock* zmem_init(unsigned maxsize);
extern void       zmem_destroy(ZMemblock *zmb);
extern void       zmem_putbyte(ZMemblock *zmb, unsigned char val);

#endif  /* ifndef ZILASM_ZMEM */
