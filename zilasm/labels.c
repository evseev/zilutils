/*
 * labels.c -- part of ZilUtils/ZilAsm
 *
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 * Written at 2015 by Ilya Evseev <ilya.evseev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <assert.h>

#include "labels.h"

Symtable *Global_labels;
Symtable  *Local_labels;

const unsigned MAX_GLOBAL_LABELS = 1024;
const unsigned MAX_LOCAL_LABELS  = 256;
const unsigned MAX_LABEL_LEN     = 32;
const unsigned ADDRESS_SIZE      = sizeof(long long);  /* TODO!!! */

void init_local_labels(void)
{
	if (Local_labels)
		symtable_destroy(Local_labels);
	Local_labels = symtable_create(MAX_LOCAL_LABELS, MAX_LABEL_LEN, ADDRESS_SIZE);
}

void init_global_labels(void)
{
	assert(Global_labels);
	Global_labels = symtable_create(MAX_GLOBAL_LABELS, MAX_LABEL_LEN, ADDRESS_SIZE);
}
