/*
 * main.c
 *
 * Copyright (C) 2015 Alexander Andrejevic <theflash AT sdf DOT lonestar DOT org>
 * Copyright (C) 2015 Jason Self <j@jxself.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <getopt.h>
#include <time.h>
#include <ctype.h>

#include "config.h"

const int DEFAULT_ZVERSION = 6;

enum { ZVERSION = 11, ZORKID, ZSERIAL };

enum { FAIL = -1, OK = 0, NEED_RESTART = 1 };

static struct option const long_options[] =
{
	{ "help",     no_argument,       NULL, 'h' },
	{ "version",  no_argument,       NULL, 'V' },
	{ "output",   required_argument, NULL, 'o' },
	{ "zversion", required_argument, NULL, ZVERSION },
	{ "zorkid",   required_argument, NULL, ZORKID   },
	{ "serial",   required_argument, NULL, ZSERIAL  },
	{ NULL, 0, NULL, 0 }
};

typedef struct {
	int todo;
} Opcode_dict;

struct
{
	int  zversion;     /* 0 - 8     */
	int  zorkid;       /* 0 - 65535 */
	char zserial[7];   /* YYMMDD    */
	Opcode_dict *opcode_dict;
} Config;

void wrong_arg(const char *err, ...)
{
	if (err) {
		va_list ap;
		va_start(ap, err);
		vfprintf(stderr, err, ap);
		va_end(ap);
	}
	fprintf(stderr, "Try `" PACKAGE_NAME " --help' for more information.\n");
	exit(1);
}

void print_version()
{
	printf( PACKAGE_STRING "\n"
	       "License AGPLv3+: GNU AGPL version 3 or later\n"
	       "<http://gnu.org/licenses/agpl.html>\n"
	       "This is free software: you are free to change and redistribute it.\n"
	       "There is NO WARRANTY, to the extent permitted by law.\n"
	);
	exit(0);
}

void print_usage(int failed)
{
	printf("Usage: " PACKAGE_NAME " [OPTION...] [FILES...]\n"
	       "\n"
	       "--version  Display program version and exit\n"
	       "--help     Display this help\n"
	       "\n"
	       "--zversion (accepts numbers 1 - 8, defaults to %d if not specified)\n"
	       "--zorkid   (integer between 0 and 65535, defaults to 0 if not specified)\n"
	       "--serial   (six characters of ASCII, defaults to current date\n"
	       "            in the form YYMMDD if not specified)\n",
	       DEFAULT_ZVERSION
	);
	exit(failed);
}

void fill_zserial(void)
{
	time_t t;
	struct tm *timeinfo;
	time (&t);
	timeinfo = localtime(&t);
	strftime (Config.zserial, sizeof(Config.zserial), "%y%m%d", timeinfo);
}

void fill_config(void)
{
	bzero(&Config, sizeof(Config));
	Config.zversion = DEFAULT_ZVERSION;
	fill_zserial();
}

void parse_intarg(int *dest, const char name[], int min, int max, int defval)
{
	if (!optarg) {
		*dest = defval;
		return;
	}
	int n = atoi(optarg);
	if (n >= min && n <= max) {
		*dest = n;
		return;
	}
	wrong_arg("Wrong %s value %s, must be integer between %d and %d\n",
		name, optarg, min, max);
}

void parse_zserial(void)
{
	if (!optarg) {
		fill_zserial();
		return;
	}
	size_t n = strlen(optarg);
	if (n == sizeof(Config.zserial) - 1) {
		char *p = optarg;
		while (*p && isalnum(*p))
			p++;
		if (!*p) {    /* ..optarg contains alphanumeric only? */
			strncpy(Config.zserial, optarg, sizeof(Config.zserial));
			return;
		}
	}
	wrong_arg("Wrong zserial value %s, must be 6 ascii characters\n", optarg);
}

void new_file_suffix(char *result, size_t maxlen, const char *src, const char *newsuffix)
{
	strncpy(result, src, maxlen);
	char *p = strrchr(result, '.');
	if (p && strchr(p, '/'))
		p = NULL;
	if (p) {
		strncpy(p, newsuffix, maxlen - (p - result));
	} else {
		strncat(result, newsuffix, maxlen);
	}
	result[maxlen] = 0;
}

char *build_output_filename(const char basename[], const char *suffix)
{
	int n = strlen(basename) + strlen(suffix);
	char *ofile = malloc(n + 1);  /* todo!!! check for NULL. free. */
	new_file_suffix(ofile, n, basename, suffix);
	return ofile;
}

int init_assembly(void)
{
	/* TODO */
	return OK;
}

int assembly(void)
{
	/* TODO */
	return OK;
}

int main(int argc, char *argv[], char *envp[])
{
	const char *output_file = NULL;
	int i;

	fill_config();

	int opt = 0;
	while ((opt = getopt_long (argc, argv, "hVo:", long_options, NULL)) != -1) {
		switch(opt) {
		case 'h'     : print_usage(0);
		case 'V'     : print_version();
		case 'o'     : if (output_file) wrong_arg("Output file must be given once\n");
		               output_file = optarg;
		               break;
		case ZVERSION: parse_intarg(&Config.zversion, "zversion", 1, 8,      1); break;
		case ZORKID  : parse_intarg(&Config.zorkid,   "zorkid",   0, 0xFFFF, 0); break;
		case ZSERIAL : parse_zserial();                                          break;
		default      : wrong_arg(0);
		}
	}

	int first_input_file = optind;
	if (first_input_file >= argc)
		wrong_arg("Missing input file\n");
	if (!output_file)
		output_file = build_output_filename(argv[first_input_file], ".dat");

	// TODO: Everything :)

	printf("Input files:\n");
	for (i = optind; i < argc; i++)
		printf("\t%s\n", argv[i]);

	printf("Output file: %s\n\n", output_file);

	printf("Config:\n"
	       "- ZVersion: %d\n"
	       "- ZorkID:   %d\n"
	       "- ZSerial:  %s\n",
	       Config.zversion, Config.zorkid, Config.zserial
	);

	init_opcodes(Config.zversion, 0);

	while(init_assembly() == OK && assembly() == NEED_RESTART);

	/* TODO! List global symbols */
	/* TODO! Find abbreviations */

	return 0;
}
